package com.devcamp.bookauthorapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainBook {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> booksLArrayList() {
        Author author1 = new Author("Nguyễn Nhật Ánh", "nna@gmail.com", 'm');
        Author author2 = new Author("Nguyễn Hiến Lê", "nhl@gmail.com", 'm');
        Author author3 = new Author("Nguyễn Tư", "nt@gmail.com", 'f');

        Book book1 = new Book("Tôi Thấy Hoa Vàng Trên Cỏ Xanh", 95000, 10, author1);
        Book book2 = new Book("Các Cuộc Đời Ngoại Hạng", 10000, 16, author2);
        Book book3 = new Book("Đi Tìm Lẻ Sống", 85000, 5, author3);
        System.out.println(book1.toString());
        System.out.println(book2.toString());
        System.out.println(book3.toString());

        ArrayList<Book> bList = new ArrayList<>();
        bList.add(book1);
        bList.add(book2);
        bList.add(book3);
        return bList;

    }

}
